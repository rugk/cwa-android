package de.rki.coronawarnapp.update

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import com.google.gson.Gson
import de.rki.coronawarnapp.BuildConfig
import de.rki.coronawarnapp.environment.BuildConfigWrap
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import okio.BufferedSink
import okio.buffer
import okio.sink
import timber.log.Timber
import java.io.File
import java.util.concurrent.TimeUnit
import java.util.zip.ZipFile

const val DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS = 3
const val UPDATE_AVAILABLE = 2
const val UPDATE_NOT_AVAILABLE = 1
const val UNKNOWN = 0

const val IMMEDIATE = 1
const val FLEXIBLE = 0

class AppUpdateManager(
    context: Context
) {
    private val internalAppUpdateInfo: AppUpdateInfo by lazy { AppUpdateInfo(context) }
    fun getAppUpdateInfo(): AppUpdateInfo {
        return internalAppUpdateInfo
    }

    fun startUpdateFlowForResult(appUpdateInfo: AppUpdateInfo, immediate: Int, activity: Activity, updateCode: Int) {
        val downloadIntent = Intent(Intent.ACTION_VIEW, Uri.parse(appUpdateInfo.downloadUrl))
        activity.startActivity(downloadIntent)
        activity.finish()
    }
}

class AppUpdateInfo(
    private val context: Context,
) {
    private var suggestedVersionCode: Long = -1
    var downloadUrl: String? = null

    init {
        try {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (cm.activeNetworkInfo?.isConnectedOrConnecting == true) {
                Timber.tag(TAG).d("Internet connected, downloading version info")
                val logging = HttpLoggingInterceptor { message -> Timber.tag(TAG).v(message) }.apply {
                    if (BuildConfig.DEBUG) setLevel(HttpLoggingInterceptor.Level.BODY)
                }
                val client = OkHttpClient.Builder()
                    .callTimeout(1, TimeUnit.SECONDS)
                    .cache(
                        Cache(
                            directory = File(context.cacheDir, "http_cache"),
                            maxSize = 1L * 1024L * 1024L // 1 MiB
                        )
                    )
                    .addInterceptor(logging)
                    .build()
                val request: Request = Request.Builder()
                    .url(BASEURL + FDROID_REPO_PATH + INDEX_FILENAME)
                    .build()

                client.newCall(request).execute().use { response ->
                    response.body?.let {
                        val tmpfile = File.createTempFile("index-v1", ".jar", context.cacheDir)
                        val sink: BufferedSink = tmpfile.sink().buffer()
                        sink.writeAll(it.source())
                        sink.close()
                        val gson = Gson()
                        val zipfile = ZipFile(tmpfile)
                        val indexEntry = zipfile.getEntry(DATA_FILE_NAME)
                        val map: Map<*, *> =
                            gson.fromJson(zipfile.getInputStream(indexEntry).reader().buffered(), Map::class.java)
                        val ourApp: Map<*, *> = (map["apps"] as List<*>)[0] as Map<*, *>
                        if (ourApp["packageName"] == context.packageName) {
                            suggestedVersionCode = (ourApp["suggestedVersionCode"] as String).toLong()
                            Timber.tag(TAG).d("got suggestedVersionCode=$suggestedVersionCode")
                            downloadUrl = "$BASEURL$FDROID_REPO_PATH${context.packageName}_$suggestedVersionCode.apk"
                            Timber.tag(TAG).d("got update downloadURL: $downloadUrl")
                        }
                        tmpfile.delete()
                    }
                }
            }
        } catch (e: Exception) {
            Timber.tag(TAG).i("AppUpdateInfo: update checking failed!")
            Timber.tag(TAG).d(e)
        }
    }

    fun updateAvailability(): Any {
        val currentVersion = BuildConfigWrap.VERSION_CODE
        Timber.tag(TAG).d("updateAvailability(): currentVersion=$currentVersion")
        Timber.tag(TAG).d("updateAvailability(): suggestedVersionCode=$suggestedVersionCode")
        if (currentVersion < suggestedVersionCode) {
            return UPDATE_AVAILABLE
        }
        return UPDATE_NOT_AVAILABLE
    }

    companion object {
        const val BASEURL = "https://bubu1.eu/cctg"
        const val FDROID_REPO_PATH = "/fdroid/repo/"
        const val INDEX_FILENAME = "index-v1.jar"
        const val DATA_FILE_NAME = "index-v1.json"
    }
}

class AppUpdateManagerFactory {
    companion object {
        fun create(context: Context): AppUpdateManager {
            return AppUpdateManager(context)
        }
    }
}

fun AppUpdateManager.getUpdateInfo(): AppUpdateInfo? {
    Timber.tag(TAG).d("getUpdateInfo()")
    return try {
        getAppUpdateInfo()
    } catch (e: Exception) {
        Timber.tag(TAG).w(e, "getUpdateInfo() failed")
        null
    }
}

private const val TAG = "InAppUpdate"
