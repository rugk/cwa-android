**Important information for CCTG users up to version 2.14.1.2 and microG users up to version 0.2.23**

### Risk calculation issue in CCTG and microG

From CCTG version 1.9.x up to and including version 2.14.1.1, and in microG versions
up until 0.2.22, an issue which affected the quality of risk assessment was present.

In [microg/#1633](https://github.com/microg/GmsCore/issues/1633), it was noticed that
the fields `reportType` and `daysSinceOnsetOfSymptoms` were not returned correctly to CCTG due to a
technical issue.

Corona-Warn-App uses the *Report Type* field, together with the *Days since Onset of Symptoms* field, to encode the *Transmission Risk* value, as
seen [in this document](https://github.com/corona-warn-app/cwa-documentation/blob/main/images/risk_calculation/client_interpretation.pdf).
(As such, these two fields do not contain the values that their names suggest they do. This is the reason
you might see the perhaps odd report types `RECURSIVE`, `CLINICAL_DIAGNOSIS`, and `SELF_REPORT` besides
`CONFIRMED_TEST` when analyzing exposures using [the Companion app](https://github.com/mh-/corona-warn-companion-android/).)

The Transmission Risk is determined by the first day that an infected person was symptomatic, which they can enter
during the process of Warning Others, described in more detail [in this document](https://github.com/corona-warn-app/cwa-documentation/blob/master/transmission_risk.pdf).

Because of the faulty implementation, all Transmission Risk levels of infected persons were treated as if they
had the level VII. Exposures that originally had a lower Transmission Risk value
were therefore weighted more heavily during risk calculation than they should have been,
or considered at all when they should not have been, in [accordance with this chart](https://github.com/corona-warn-app/cwa-documentation/blob/master/images/risk_calculation/risk_calculation_enf_v2_overview.pdf).
In the same way, exposures that originally had Transmission Risk level VIII were weighted slightly less than
they should have been.

Therefore, **some of your red warnings inside CCTG might have been caused by exposures that should not have
caused such a warning** as a result of this incorrect implementation.

We sincerely apologize for any inconvenience caused to our users due to unnecessary warnings.

We have made available patched versions that no longer contain this issue. Unfortunately, the first patched version we released contained a new fault, discovered in [microg/#1655](https://github.com/microg/GmsCore/issues/1655), affecting CCTG version 2.14.1.2 and microG version 0.2.23. If you are using those versions, please also update as follows.

* If you are using **internal microG**: please update to **CCTG version 2.14.1.3**, available now on https://bubu1.eu/cctg.
* If you are using **external microG**: please update to **microG version 0.2.24**, available now on https://github.com/microg/GmsCore/releases/tag/v0.2.24.214816.
* Whether you are using external microG is indicated at the bottom of the App Information screen.


Risk calculations during the first 14 days after updating may still be affected, since the `reportType` field
will only be set correctly for newly downloaded diagnosis keys after the update.

* Using Corona-Warn-App with affected microG versions is also affected, as the issue lies within microG.

### Fehlerhafte Risikoberechnung mit CCTG und microG

Seit CCTG-Version 1.9.x bis einschließlich Version 2.14.1.1, und bis einschließlich microG-Version 0.2.22, lag in
der App ein Fehler vor, der die Qualität der Risikoberechnung beeinträchtigt hat.

Seitens microG wurde in [microg/#1633](https://github.com/microg/GmsCore/issues/1633) festgestellt, dass die Felder `reportType`
sowohl als auch `daysSinceOnsetOfSymptoms` aufgrund eines technischen Fehlers nicht korrekt an CCTG zurückgegeben wurden.

In der Corona-Warn-App werden das Feld *Report Type* sowie *Tage seit Symptomeintritt* gemeinsam genutzt, um das
Übertragungsrisiko (*Transmission Risk*) zu codieren, wie [in diesem Dokument](https://github.com/corona-warn-app/cwa-documentation/blob/main/images/risk_calculation/client_interpretation.pdf)
gezeigt. (Die Felder enthalten also nicht den Wert, den ihr Name vermuten lässt.
Aus diesem Grund kommt es vor, dass man in [der Companion-App](https://github.com/mh-/corona-warn-companion-android/)
mitunter die wohl merkwürdigen Report Types `RECURSIVE`, `CLINICAL_DIAGNOSIS`, und `SELF_REPORT` nebst
`CONFIRMED_TEST` sehen kann, wenn man seine Begegnungen dort analysiert.)

Das Übertragungsrisiko entscheidet sich anhand des Tages, an dem bei der infizierte Person erstmalig Symptome aufgetreten sind.
Diesen Wert kann sie eingeben, während sie Andere Warnt. Die genaue Festlegung
des Übertragungsrisikos erschließt sich [aus diesem Dokument](https://github.com/corona-warn-app/cwa-documentation/blob/master/transmission_risk.pdf).

Aufgrund der fehlerhaften Implementierung wurden alle Risikobegegnungen so behandelt, als hätten sie das
Übertragungsrisiko VII. Risikobegegnungen, die ursprünglich ein niedrigeres Übertragungsrisiko
hatten, wurden daher stärker gewichtet als vorgesehen, beziehungsweise in die Rechnung mit einbezogen, wenn sie
nicht hätten berücksichtigt werden sollen. Die Faktoren der unterschiedlichen Übertragungsrisiken werden in
[dieser Graphik ersichtlich](https://github.com/corona-warn-app/cwa-documentation/blob/master/images/risk_calculation/risk_calculation_enf_v2_overview.pdf).
Auf die gleiche Weise wurden Begegnungen des Übertragungsrisikos VIII etwas zu wenig gewichtet.

**Daher können manche der roten Risikowarnungen in Ihrer CCTG ausgelöst worden sein durch Begegnungen, die keine solche
Warnung hätten verursachen sollen**. Wir entschuldigen uns für alle Unannehmlichkeiten, die Ihnen aufgrund von unnötigen Warnungen entstanden sind.

Wir haben neue Versionen bereitgestellt, in denen das Problem behoben ist. Leider ist bei der Erstellung der ersten Version mit Problembehebung ein neuer Fehler unterlaufen, der in [microg/#1655](https://github.com/microg/GmsCore/issues/1655) gefunden wurde. Letzteres betrifft CCTG-Version 2.14.1.2 und microG-Version 0.2.23. Wenn Sie eine dieser beiden Versionen verwenden, aktualisieren Sie bitte ebenfalls wie folgt.

* Wenn Sie **internes microG** nutzen, aktualisieren Sie bitte auf **CCTG-Version 2.14.1.3**, die ab sofort unter https://bubu1.eu/cctg verfügbar ist.
* Wenn Sie **externes microG** nutzen, aktualisieren Sie bitte auf **microG-Version 0.2.24**, die ab sofort unter https://github.com/microg/GmsCore/releases/tag/v0.2.24.214816 verfügbar ist.
* Ob Sie externes microG verwenden, können Sie unten auf dem Bildschirm "App-Informationen" erkennen.


Die Risikoberechnung kann innerhalb der ersten 14 Tage nach der Aktualisierung noch beeinträchtigt sein, da das `reportType`-Feld nur für Diagnoseschlüssel gesetzt werden, die nach der Aktualisierung heruntergeladen werden.

* Die Nutzung der Corona-Warn-App mit betroffenen microG-Versionen ist ebenfalls betroffen, da das Problem in microG lag.
